package mx.fornova.goclick;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.content.SharedPreferences;
import mx.fornova.goclick.LoginActivity;
import static mx.fornova.goclick.Utils.Utils.pref;
import static mx.fornova.goclick.Utils.Utils.user_logged;

public class SplashScreenActivity extends AppCompatActivity {
    public Boolean isLogged;
    public Boolean isFirstTime;

    private static int SPLASH_TIME_OUT = 2000;
    private static SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        pref = getApplicationContext().getSharedPreferences("ProfuturoSharedPref", 0); // 0 - for private mode
        isLogged = pref.getBoolean(user_logged, false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isLogged == true) {

                    loadApp(SplashScreenActivity.this);

                } else goToLogin();

            }
        }, SPLASH_TIME_OUT);


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                goToLogin();
//
//            }
//        }, SPLASH_TIME_OUT);

    }

    public static void loadApp(Activity paramActivity) {
        paramActivity.startActivity(new Intent(paramActivity, MainActivity.class));
        paramActivity.finish();
    }

    public void goToLogin() {
        Intent n = new Intent().setClass(SplashScreenActivity.this, LoginActivity.class);
        startActivity(n);
        finish();
    }
}
