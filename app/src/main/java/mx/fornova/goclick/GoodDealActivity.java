package mx.fornova.goclick;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import mx.fornova.goclick.Utils.CShowProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static mx.fornova.goclick.Utils.Utils.DELIVERY_RATE;
import static mx.fornova.goclick.Utils.Utils.URL_BASE;
import static mx.fornova.goclick.Utils.Utils.actualDelivery;

public class GoodDealActivity extends AppCompatActivity implements View.OnClickListener {
    private Button finish_delivery;
    private EditText commentEditText;
    private RatingBar rating_service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_deal);
        setUpMenuActions();
        finish_delivery = ((Button) findViewById(R.id.finish_delivery));
        finish_delivery.setOnClickListener(this);
        commentEditText = (EditText) findViewById(R.id.commentEditText);
        rating_service = (RatingBar) findViewById(R.id.rating_service);
    }

    public void setUpMenuActions() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar.setTitle(null);
        toolbar.setLogo(null);
        toolbar.setNavigationIcon(R.drawable.back_wit);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.finish_delivery:
                if (validate()) {
                    rateService();
                } else {
                    startActivity(new Intent(this, EndDeliveryActivity.class));
                    finish();
                }

                break;
        }
    }

    private boolean validate() {
        if (commentEditText.getText().toString().equalsIgnoreCase("") && rating_service.getRating() == 0)
            return false;

        return true;
    }

    public void rateService() {
        String url = null;
        JSONObject setRequest = new JSONObject();

        url = URL_BASE + DELIVERY_RATE;
        try {
            String comment = commentEditText.getText().toString();
            String request = actualDelivery.getId();
            String score = String.valueOf(rating_service.getRating());
            setRequest.put("comment", comment);
            setRequest.put("isClient", false);
            setRequest.put("requestId", request);
            setRequest.put("score", score);
        } catch (JSONException e) {
        }


        final CShowProgress cShowProgress = CShowProgress.getInstance();
        cShowProgress.showProgress(GoodDealActivity.this);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, setRequest,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response;
                            Integer status = jsonObject.getInt("status");
                            if (status != null) {
                                if (status == 0) {
                                    startActivity(new Intent(GoodDealActivity.this, EndDeliveryActivity.class));
                                    finish();
                                } else if (status == 1) {
                                    Toast.makeText(GoodDealActivity.this, jsonObject.getString("errors"), Toast.LENGTH_LONG).show();
                                } else if (status == 2) {
                                    Toast.makeText(GoodDealActivity.this, getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
                                }
                            }
                            cShowProgress.hideProgress();

                        } catch (JSONException e) {

                            cShowProgress.hideProgress();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                cShowProgress.hideProgress();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        queue.add(jsonObjReq);
    }
}
