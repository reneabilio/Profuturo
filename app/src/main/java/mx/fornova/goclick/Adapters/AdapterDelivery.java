package mx.fornova.goclick.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import mx.fornova.goclick.Models.Delivery;
import mx.fornova.goclick.R;
//import com.wit.delivery.Services.SocketLocationService1;
//import com.wit.delivery.StatusDeliveryActivity;
import mx.fornova.goclick.StatusDeliveryActivity;
import mx.fornova.goclick.Utils.GraphicsUtil;

import java.util.List;

//import static com.wit.delivery.Utils.Utils.InsertDeliveryToSocketLocationService;
import static mx.fornova.goclick.Utils.Utils.actualDelivery;

public class AdapterDelivery
        extends BaseAdapter {
    private final Activity context;
    private final List<Delivery> deliveries;
    private final LayoutInflater mInflater;

    public AdapterDelivery(Activity paramActivity, List<Delivery> paramList) {
        context = paramActivity;
        deliveries = paramList;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    private void go_to_on_delivery() {
//        InsertDeliveryToSocketLocationService(actualDelivery);
//        context.stopService(new Intent(context, SocketLocationService1.class));
//
//        context.startService(new Intent(context, SocketLocationService1.class));
        this.context.startActivity(new Intent(this.context, StatusDeliveryActivity.class));
    }

    @Override
    public int getCount() {
        return deliveries.size();
    }

    @Override
    public Object getItem(int position) {
        return deliveries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() > 0) {
            return getCount();
        } else {
            return super.getViewTypeCount();
        }
    }

    public View getView(final int posicion, View view, ViewGroup parent) {

        ViewHolder holder = null;

        if (view == null) {

            view = mInflater.inflate(R.layout.item_list_delivery, parent, false);

            holder = new ViewHolder(view);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//        view.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View paramAnonymousView) {
//                Toast.makeText(AdapterDelivery.this.context, ((Delivery) AdapterDelivery.this.deliveries.get(paramInt)).getName(), 1).show();
//            }
//        });

        holder.name.setText(deliveries.get(posicion).getTitleCantProducts());

        if (deliveries.get(posicion).getEncoded_id() != null) {
            holder.purchase_code_num.setText(deliveries.get(posicion).getEncoded_id());
        }
        if (deliveries.get(posicion).getStripToDelivery() != null) {
            holder.address_text.setText(deliveries.get(posicion).getStripToDelivery());
        }
        if (deliveries.get(posicion).getPhone() != null) {
            holder.phone_text.setText(deliveries.get(posicion).getPhone());
        }

        holder.go.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                actualDelivery = deliveries.get(posicion);
                go_to_on_delivery();
            }
        });
        holder.go_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramAnonymousView) {
                actualDelivery = deliveries.get(posicion);
                go_to_on_delivery();
            }
        });

        if (deliveries.get(posicion).getUser_photo() != null && !deliveries.get(posicion).getUser_photo().equalsIgnoreCase("")) {
            final ViewHolder finalHolder = holder;
            Picasso.with(context).load(deliveries.get(posicion).getUser_photo()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
//                Resources res = getResources();
//                RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res, bitmap);
//                roundedBitmapDrawable.setCornerRadius(Math.min(bitmap.getHeight(), bitmap.getWidth()) / 2.0f);
//                delivery_pict.setBackgroundResource(R.drawable.transparent_box);
//                delivery_pict.setImageDrawable(roundedBitmapDrawable);
                    GraphicsUtil graphicUtil = new GraphicsUtil();
                    finalHolder.client_pict.setImageBitmap(graphicUtil.getCircleBitmap(bitmap, 16));
                }

                @Override
                public void onBitmapFailed(Drawable drawable) {
                    finalHolder.client_pict.setBackgroundResource(R.drawable.picture_face_dummy);

                }

                @Override
                public void onPrepareLoad(Drawable drawable) {
                    finalHolder.client_pict.setBackgroundResource(R.drawable.picture_face_dummy);
                }
            });
        }
        return view;
//        paramViewGroup = (ViewHolder) paramView.getTag();

    }


    class ViewHolder {
        TextView address_text;
        Button go_button;
        ImageView client_pict;
        RelativeLayout go;
        TextView name;
        TextView phone_text;
        TextView purchase_code_num;

        public ViewHolder(View paramView) {
            //this.client_pict = ((ImageView) paramView.findViewById(R.id.client_pict));
            this.purchase_code_num = ((TextView) paramView.findViewById(R.id.textView_code));
            this.name = ((TextView) paramView.findViewById(R.id.name));
            this.address_text = ((TextView) paramView.findViewById(R.id.address_text));
            this.phone_text = ((TextView) paramView.findViewById(R.id.phone_text));
            this.go = ((RelativeLayout) paramView.findViewById(R.id.go));
            this.go_button = ((Button) paramView.findViewById(R.id.go_button));
        }
    }
}


/* Location:              D:\delivery\dex2jar-2.0\dex2jar-2.0\classes2-dex2jar.jar!\com\wit\delivery\Adapters\AdapterDelivery.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */