package mx.fornova.goclick.Models;

public class Delivery
{
  private String strip_to_delivery;
  private String clientEmail;
  private String encoded_id;
  private String id;
  private String location;
  private String name;
  private String phone;
  private String user_photo;
  private Product[] products;
  
  public Delivery() {}
  
  public Delivery(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, Product[] paramString9)
  {
    this.user_photo = paramString1;
    this.name = paramString2;
    this.strip_to_delivery = paramString3;
    this.location = paramString4;
    this.phone = paramString5;
    this.id = paramString6;
    this.clientEmail = paramString7;
    this.encoded_id = paramString8;
    this.products = paramString9;
  }
  
  public String getStripToDelivery()
  {
    return this.strip_to_delivery;
  }
  
  public String getClientEmail()
  {
    return this.clientEmail;
  }
  
  public String getEncoded_id()
  {
    return this.encoded_id;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getLocation()
  {
    return this.location;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getUser_photo()
  {
    return this.user_photo;
  }

  public Product[] getProducts()
  {
    return this.products;
  }
  
  public void setStripToDelivery(String paramString)
  {
    this.strip_to_delivery = paramString;
  }
  
  public void setClientEmail(String paramString)
  {
    this.clientEmail = paramString;
  }
  
  public void setEncoded_id(String paramString)
  {
    this.encoded_id = paramString;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setLocation(String paramString)
  {
    this.location = paramString;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setUser_photo(String paramString)
  {
    this.user_photo = paramString;
  }

  public void setProducts(Product[] paramString)
  {
    this.products = paramString;
  }

  public String getTitleCantProducts(){
    String titleProds = "Pedido: ";
    if(this.products != null){
      titleProds += this.products.length + " producto(s)";
    }
    return titleProds;
  }
}


/* Location:              D:\delivery\dex2jar-2.0\dex2jar-2.0\classes2-dex2jar.jar!\com\wit\delivery\Models\Delivery.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */