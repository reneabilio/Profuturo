package mx.fornova.goclick.Models;

public class Product
{
  private String id;
  private String photo;
  private String description;
  private String quantity;
  private String price;
  private String product_qr;

  public Product() {}

  public Product(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.id = paramString1;
    this.photo = paramString2;
    this.description = paramString3;
    this.quantity = paramString4;
    this.price = paramString5;
    this.product_qr = paramString6;
  }

  public String getId()
  {
    return this.id;
  }

  public String getPhoto()
  {
    return this.photo;
  }

  public String getDescription()
  {
    return this.description;
  }

  public String getQuantity()
  {
    return this.quantity;
  }

  public String getPrice()
  {
    return this.price;
  }

  public String getProduct_qr()
  {
    return this.product_qr;
  }

  public void setId(String paramString)
  {
    this.id = paramString;
  }

  public void setPhoto(String paramString)
  {
    this.photo = paramString;
  }

  public void setDescription(String paramString)
  {
    this.description = paramString;
  }

  public void setQuantity(String paramString)
  {
    this.quantity = paramString;
  }

  public void setPrice(String paramString)
  {
    this.price = paramString;
  }

  public void setProduct_qr(String paramString)
  {
    this.product_qr = paramString;
  }
}


/* Location:              D:\delivery\dex2jar-2.0\dex2jar-2.0\classes2-dex2jar.jar!\com\wit\delivery\Models\Delivery.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */