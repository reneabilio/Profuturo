package mx.fornova.goclick.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;

import mx.fornova.goclick.Models.Delivery;

public class Utils {

    public static final String URL_BASE = "http://profuturo.theoutsiders.mx/api-v1.0";
    public static final String URL_BASE_PICT = "https://sandbox.surz.io/";
    public static final String URL_SOCKET = "http://54.191.27.210:802";

    public static final String API_SERVICE_LOGIN = "auth/login";
    public static final String REQUEST_BY_USER = "/delivery/requestsByUser";
    public static final String API_SERVICE_REGISTER = "auth/register";
    public static final String CLOSE_REQUEST = "/delivery/closeRequest";
    public static final String GET_USER_PHONE = "/user/phone";
    public static final String LOGIN = "/user/login";
    public static final String REGISTER_USER = "/user/register";
    public static final String REQUEST_BY_DISTRIBUTORS = "/distributor/requestsByDistributor";
    public static final String RESEND_PIN = "/user/resendpin";
    public static final String DELIVERY_RATE = "/delivery/rate";
    public static final String UPDATE_DEVICE = URL_BASE + "/user/updateUserDevice";
    public static Delivery actualDelivery;
    public static List<Delivery> actualDeliveryList = new ArrayList<>();

    public static final String userID = "userID";
    public static final String user_email = "user_email";
    public static final String user_logged = "user_logged";
    public static final String user_name = "userName";
    public static final String user_phone = "user_phone";
    public static final String user_phone_pin = "user_phone_pin";
    public static final String user_phone_validated = "user_phone_validated";
    public static final String user_picture = "picture";
    public static final String user_promo_code = "promo_code";

    public static SharedPreferences pref;
    public static SharedPreferences getPref() {
        return pref;
    }

    public static void setPref(SharedPreferences paramSharedPreferences) {
        pref = paramSharedPreferences;
    }

    public static Bitmap resizeMapIcons(Context paramContext, String paramString, int paramInt1, int paramInt2) {
        return Bitmap.createScaledBitmap(BitmapFactory.decodeResource(paramContext.getResources(), paramContext.getResources().getIdentifier(paramString, "drawable", paramContext.getPackageName())), paramInt1, paramInt2, false);
    }
}
