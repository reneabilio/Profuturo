package mx.fornova.goclick;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.fornova.goclick.Adapters.AdapterDelivery;
import mx.fornova.goclick.Models.Delivery;
import static mx.fornova.goclick.Utils.Utils.pref;
import static mx.fornova.goclick.Utils.Utils.REQUEST_BY_USER;
import static mx.fornova.goclick.Utils.Utils.URL_BASE;
import static mx.fornova.goclick.Utils.Utils.user_email;
import static mx.fornova.goclick.Utils.Utils.URL_BASE_PICT;
//import static mx.fornova.goclick.Utils.Utils.deliveryList;
import mx.fornova.goclick.Utils.CShowProgress;
import mx.fornova.goclick.Utils.GraphicsUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView name;
    private TextView phone_user_text;
    private TextView transport_text;
    private ImageView delivery_pict;
    private SwipeRefreshLayout pullToRefresh;
    private Boolean callService = true;
    public static List<Delivery> deliveryList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        loadMyDeliveries();
        String name = pref.getString("userName", "");
        String phone = pref.getString("user_phone", "");
        String user_pict = pref.getString("picture", "");
        String url = URL_BASE_PICT + user_pict;
        pref.getString("user_phone", "");

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub

                if (callService) {
                    loadMyDeliveries();
                }

            }
        });

        this.name = ((TextView) findViewById(R.id.textview_name));
        this.name.setText(name);
        this.phone_user_text = ((TextView) findViewById(R.id.textview_phone));
        this.transport_text = ((TextView) findViewById(R.id.textview_transport));
        this.delivery_pict = ((ImageView) findViewById(R.id.delivery_pict));
        this.phone_user_text.setText(phone);
        this.transport_text.setText("Car");
        Picasso.with(this).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
//                Resources res = getResources();
//                RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res, bitmap);
//                roundedBitmapDrawable.setCornerRadius(Math.min(bitmap.getHeight(), bitmap.getWidth()) / 2.0f);
//                delivery_pict.setBackgroundResource(R.drawable.transparent_box);
//                delivery_pict.setImageDrawable(roundedBitmapDrawable);
                GraphicsUtil graphicUtil = new GraphicsUtil();
                delivery_pict.setImageBitmap(graphicUtil.getCircleBitmap(bitmap, 16));
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {
                delivery_pict.setBackgroundResource(R.drawable.picture_face_dummy);

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {
                delivery_pict.setBackgroundResource(R.drawable.picture_face_dummy);
            }
        });
    }

    public void logout() {
        pref = getApplicationContext().getSharedPreferences("SurzClickeaDeliverySharedPref", 0);
        SharedPreferences.Editor localEditor = pref.edit();
        localEditor.remove("user_logged");
        localEditor.commit();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_disconnect:
                logout();
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadMyDeliveries();
    }

    private void loadMyDeliveries() {
        String url = null;

        JSONObject setRequest = new JSONObject();

        url = URL_BASE + REQUEST_BY_USER;
        String mail = pref.getString(user_email, "");
        try {
            setRequest.put("email", mail);
        } catch (JSONException e) {
        }
        pullToRefresh.setRefreshing(false);
        final CShowProgress cShowProgress = CShowProgress.getInstance();
        cShowProgress.showProgress(MainActivity.this);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, setRequest,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response;
                            Integer status = jsonObject.getInt("status");
                            if (status != null) {
                                if (status == 0) {
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    JSONArray data = jsonObjectData.getJSONArray("requests");

                                    deliveryList.clear();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject delivery = data.getJSONObject(i);
                                        Gson gson = new Gson();
                                        String jsonInString = delivery.toString();
                                        Delivery d = gson.fromJson(jsonInString, Delivery.class);
                                        deliveryList.add(d);
                                    }
                                    setAdapterList();
                                } else if (status == 1) {
                                    Toast.makeText(MainActivity.this, jsonObject.getString("errors"), Toast.LENGTH_LONG).show();
                                } else if (status == 2) {
                                    Toast.makeText(MainActivity.this, getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
                                }

                            }
                            callService = true;
                            cShowProgress.hideProgress();

                        } catch (JSONException e) {

                            cShowProgress.hideProgress();
                            callService = true;

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                cShowProgress.hideProgress();
                callService = true;

            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        queue.add(jsonObjReq);
    }

    private void setAdapterList() {
        AdapterDelivery localAdapterDelivery = new AdapterDelivery(this, deliveryList);
        ((ListView) findViewById(R.id.listview)).setAdapter(localAdapterDelivery);
    }
}
