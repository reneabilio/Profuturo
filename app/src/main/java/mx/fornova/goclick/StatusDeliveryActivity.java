package mx.fornova.goclick;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.engineio.client.Socket;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import mx.fornova.goclick.Utils.CShowProgress;
import mx.fornova.goclick.Utils.Route;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static mx.fornova.goclick.Utils.Utils.CLOSE_REQUEST;
import static mx.fornova.goclick.Utils.Utils.URL_BASE;
import static mx.fornova.goclick.Utils.Utils.URL_SOCKET;
import static mx.fornova.goclick.Utils.Utils.actualDelivery;
import static mx.fornova.goclick.Utils.Utils.actualDeliveryList;
import static mx.fornova.goclick.Utils.Utils.pref;
import static mx.fornova.goclick.Utils.Utils.resizeMapIcons;
import static mx.fornova.goclick.Utils.Utils.user_email;

//public class StatusDeliveryActivity extends AppCompatActivity implements View.OnClickListener {
public class StatusDeliveryActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, LocationSource.OnLocationChangedListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private TextView address_client;
    private ImageView assigned_state_image;
    private Button call;
    private Button cancel;
    public Marker customPositionMarker = null;
    boolean firsttime = true;
    private ImageView in_place_state;
    private LatLng latLngDelivery = null;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private LatLng myPosition;
    private TextView name;
    private ImageView on_route_state_image;
    private ImageView packed_state;
    private TextView phone_user_text;
    private Toolbar toolbar;
    private ImageView user_pict;
    private RelativeLayout layout_delivery_data;
    private Route rt;
    private String idQrRequest = null;
    public static SupportMapFragment mapFragment;
    //private Socket mSocket;

    {
        {
//            try {
//                mSocket = IO.socket(URL_SOCKET);
//            } catch (URISyntaxException e) {
//            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_delivery);
        setUpMenuActions();

        getActualDeliveryLocation();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        if ((ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION
                    , android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        mLocationRequest = LocationRequest.create();

        mLocationRequest.setInterval(5000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(1000);


        packed_state = (ImageView) findViewById(R.id.packed_state);
        assigned_state_image = (ImageView) findViewById(R.id.assigned_state_image);
        on_route_state_image = (ImageView) findViewById(R.id.on_route_state_image);
        in_place_state = (ImageView) findViewById(R.id.in_place_state);
        user_pict = (ImageView) findViewById(R.id.user_pict);
        name = (TextView) findViewById(R.id.name);
        phone_user_text = (TextView) findViewById(R.id.phone_user_text);
        address_client = (TextView) findViewById(R.id.address_client);
        layout_delivery_data = (RelativeLayout) findViewById(R.id.layout_delivery_data);

        if (actualDelivery.getName() != null)
            name.setText(actualDelivery.getName());
        if (actualDelivery.getPhone() != null)
            phone_user_text.setText(actualDelivery.getPhone());
        if (actualDelivery.getStripToDelivery() != null)
            address_client.setText(actualDelivery.getStripToDelivery());
        if (actualDelivery.getUser_photo() != null)
            if (!actualDelivery.getUser_photo().equalsIgnoreCase("")) {
                String photo_url = actualDelivery.getUser_photo();
                Picasso.with(StatusDeliveryActivity.this)
                        .load(photo_url)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                    Resources res = getResources();
//                                    RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res,bitmap);
//                                    roundedBitmapDrawable.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);
//                                    user_pict.setImageDrawable(roundedBitmapDrawable);
////                                    user_pict.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                Resources res = getResources();
                                RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res, bitmap);
                                roundedBitmapDrawable.setCornerRadius(Math.max(bitmap.getHeight(), bitmap.getWidth()) / 2.0f);
                                user_pict.setBackgroundResource(R.drawable.transparent_box);
                                user_pict.setImageDrawable(roundedBitmapDrawable);
//                                    user_pict.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable drawable) {
                                user_pict.setBackgroundResource(R.drawable.picture_face_order_dummy);

                            }

                            @Override
                            public void onPrepareLoad(Drawable drawable) {
                                user_pict.setBackgroundResource(R.drawable.picture_face_order_dummy);
                            }

                        });
//                    Picasso.with(StatusDeliveryActivity.this).load(actualDelivery.getUser_photo()).into(user_pict);
            } else {
                user_pict.setImageResource(R.drawable.picture_face_order_dummy);
            }

    }

    @Override
    protected void onResume() {
        super.onResume();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void setUpMenuActions() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar.setTitle(null);
        toolbar.setLogo(null);
        toolbar.setNavigationIcon(R.drawable.back_wit);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scan_qr:
                goToScan();
                break;
            case R.id.make_call:
                makeCall(actualDelivery.getPhone());
                break;
            case R.id.cancel:
                break;
            case R.id.packed_state:
                break;
            case R.id.assigned_state_image:
                setState(R.id.assigned_state_image);
                break;
            case R.id.on_route_state_image:
                if (assigned_state_image.isEnabled() == false)
                    setState(R.id.on_route_state_image);
                break;
            case R.id.in_place_state:
                if (on_route_state_image.isEnabled() == false) {
                    setState(R.id.in_place_state);
                    startActivity(new Intent(StatusDeliveryActivity.this, OnDeliveryActivity.class));
                    finish();
                }
                break;


        }
    }

    public void makeCall(String telefono) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(
                    StatusDeliveryActivity.this, android.Manifest.permission.CALL_PHONE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                Log.i("Mensaje", "No se tiene permiso para realizar llamadas telefónicas.");

                ActivityCompat.requestPermissions(StatusDeliveryActivity.this, new String[]{android.Manifest.permission.CALL_PHONE}, 225);
            } else {
                Log.i("Mensaje", "Se tiene permiso!");

                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + telefono));

                if (ActivityCompat.checkSelfPermission(StatusDeliveryActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }

        } else {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + telefono));

            if (ActivityCompat.checkSelfPermission(StatusDeliveryActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);

        }


    }

    private void getActualDeliveryLocation() {
        if ((actualDelivery.getLocation() != null) && (!actualDelivery.getLocation().equalsIgnoreCase(""))) {
            String localObject = actualDelivery.getLocation();
            Double localDouble = Double.valueOf(localObject.substring(0, localObject.indexOf(",")));
            localObject = localObject.substring(localObject.indexOf(",") + 1, localObject.length());
            latLngDelivery = new LatLng(localDouble, Double.valueOf(localObject));
        }
    }

    private void goToScan() {
//        startActivity(new Intent(this, OnDeliveryActivity.class));
//        finish();
        new IntentIntegrator(this).setCaptureActivity(ScanQRActivity.class).setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES).initiateScan();
    }

    @Override
    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        IntentResult localIntentResult = IntentIntegrator.parseActivityResult(paramInt1, paramInt2, paramIntent);
        if (localIntentResult != null) {
            if (localIntentResult.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_SHORT).show();
                return;
            }
            String contenResult = localIntentResult.getContents();
            idQrRequest = contenResult.substring(0, contenResult.indexOf("|"));
            callCloseRequestService();
            return;
        }
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
    }

    private void callCloseRequestService() {
        String url = null;

        JSONObject setRequest = new JSONObject();

        url = URL_BASE + CLOSE_REQUEST;
        String mail = pref.getString(user_email, "");
        try {
            setRequest.put("email", mail);
            setRequest.put("id", idQrRequest);
        } catch (JSONException e) {
        }
        final CShowProgress cShowProgress = CShowProgress.getInstance();
        cShowProgress.showProgress(StatusDeliveryActivity.this);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, setRequest,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response;
                            Integer status = jsonObject.getInt("status");
                            if (status != null) {
                                if (status == 0) {
                                    startActivity(new Intent(StatusDeliveryActivity.this, GoodDealActivity.class));
                                    createSocketConnection();
                                } else if (status == 1) {
                                    Toast.makeText(StatusDeliveryActivity.this, jsonObject.getString("errors"), Toast.LENGTH_LONG).show();
                                } else if (status == 2) {
                                    Toast.makeText(StatusDeliveryActivity.this, getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
                                }

                            }
                            cShowProgress.hideProgress();

                        } catch (JSONException e) {

                            cShowProgress.hideProgress();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                cShowProgress.hideProgress();
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);
        queue.add(jsonObjReq);
    }

    private void createSocketConnection() {
//        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//
//            @Override
//            public void call(Object... args) {
//                JSONObject prov = new JSONObject();
//
//                try {
//                    String mail = actualDelivery.getClientEmail();
//                    Integer order = Integer.valueOf(actualDelivery.getId());
//                    prov.putOpt("to", mail);
//                    prov.putOpt("order", order);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                mSocket.emit("on_read_qr", prov);
//                for (int i = 0; i < actualDeliveryList.size(); i++) {
//                    if (actualDelivery.getId().equalsIgnoreCase(actualDeliveryList.get(i).getId())) {
//                        actualDeliveryList.remove(i);
//                        break;
//                    }
//                }
//                if (actualDeliveryList.size() == 0)
//                    stopService(new Intent(StatusDeliveryActivity.this, SocketLocationService1.class));
//                finish();
//
//            }
//
//        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//
//            @Override
//            public void call(Object... args) {
//
//            }
//
//        });
//
//        mSocket.connect();
    }

//    @Override
//    protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
//        IntentResult localIntentResult = IntentIntegrator.parseActivityResult(paramInt1, paramInt2, paramIntent);
//        if (localIntentResult != null)
//        {
//            if (localIntentResult.getContents() == null)
//            {
//                Toast.makeText(this, "Result Not Found", 1).show();
//                return;
//            }
//            try
//            {
//                //paramIntent = new JSONObject(localIntentResult.getContents());
////                id_Request_Order = paramIntent.getString("name");
////                id_Request_Order = paramIntent.getString("name");
////                nameQR = paramIntent.getString("name");
////                addressQR = paramIntent.getString("address");
////                Toast.makeText(this, nameQR + "  " + addressQR, Toast.LENGTH_SHORT).show();
////                return;
//            }
//            catch (JSONException paramIntent)
//            {
//                paramIntent.printStackTrace();
//                Toast.makeText(this, localIntentResult.getContents(), Toast.LENGTH_SHORT).show();
//                return;
//            }
//        }
//        super.onActivityResult(paramInt1, paramInt2, paramIntent);
//    }


    private void setState(int paramInt) {
        if (paramInt == R.id.assigned_state_image) {
            this.assigned_state_image.setBackgroundResource(R.drawable.on_assigned_on);
            this.assigned_state_image.setEnabled(false);
            this.packed_state.setBackgroundResource(R.drawable.on_packed_done);
            this.packed_state.setEnabled(false);
        }
        if (paramInt == R.id.on_route_state_image) {
            this.on_route_state_image.setBackgroundResource(R.drawable.on_route_on);
            this.assigned_state_image.setBackgroundResource(R.drawable.on_assigned_done);
            this.assigned_state_image.setEnabled(false);
            this.on_route_state_image.setEnabled(false);
        }
        if (paramInt == R.id.in_place_state) {
            this.in_place_state.setBackgroundResource(R.drawable.on_inplace_on);
            this.on_route_state_image.setBackgroundResource(R.drawable.on_route_done);
            this.on_route_state_image.setEnabled(false);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // Update location every second

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        myPosition = currentLatLng;

        mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));

        if (customPositionMarker == null) {
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));
//            mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
            customPositionMarker = mMap.addMarker(new MarkerOptions()
                    .position(myPosition)
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(StatusDeliveryActivity.this, "own_position", 80, 80))));
        } else {
            customPositionMarker.setPosition(myPosition);
        }


        if (myPosition != null && latLngDelivery != null) {
            if (rt != null)
                rt.clearRoute();
            rt = new Route();
            rt.drawRoute(mMap, StatusDeliveryActivity.this, myPosition,
                    latLngDelivery, false, "es");
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        //mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        //mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        if (statusOfGPS == false) {
            gpsDialog();
        }
        if (latLngDelivery != null) {
            Marker providerMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLngDelivery)
                    .title(actualDelivery.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(StatusDeliveryActivity.this, "delivery_to", 80, 80))));
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

    public void gpsDialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);


        adb.setTitle("Profuturo requiere activar GPS.");

        adb.setPositiveButton("Activar ahora", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                turnGPSOn();

            }
        });


        adb.setNegativeButton("Activar luego", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }

    public void turnGPSOn() {
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }
}
